package ru.yaleksandrova.tm.component;

import ru.yaleksandrova.tm.api.ICommandController;
import ru.yaleksandrova.tm.api.ICommandService;
import ru.yaleksandrova.tm.constant.ApplicationConst;
import ru.yaleksandrova.tm.constant.ArgumentConst;
import ru.yaleksandrova.tm.controller.CommandController;
import ru.yaleksandrova.tm.repository.CommandRepository;
import ru.yaleksandrova.tm.service.CommandService;

import java.util.Scanner;

public class Bootstrap {

    private final CommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController =new CommandController(commandService);

    public void start(String[] args) {
        System.out.println("** Welcome to Task Manager **");
        parseArgs(args);
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            parseCommand(command);
        }
    }

    public void parseArg(final String arg) {
        switch(arg) {
            case ArgumentConst.ABOUT: commandController.showAbout(); break;
            case ArgumentConst.VERSION: commandController.showVersion(); break;
            case ArgumentConst.HELP: commandController.showHelp(); break;
            case ArgumentConst.INFO: commandController.showInfo(); break;
            default: commandController.showErrorArgument();
        }
    }

    public void parseCommand(final String command) {
        switch(command) {
            case ApplicationConst.ABOUT: commandController.showAbout(); break;
            case ApplicationConst.VERSION: commandController.showVersion(); break;
            case ApplicationConst.HELP: commandController.showHelp(); break;
            case ApplicationConst.INFO: commandController.showInfo(); break;
            case ApplicationConst.EXIT: commandController.exitApplication(); break;
            case ApplicationConst.COMMANDS: commandController.showCommands(); break;
            case ApplicationConst.ARGUMENTS: commandController.showArguments(); break;
            default: commandController.showErrorCommand();
        }
    }

    public void  parseArgs(String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArg(arg);
    }

}
