package ru.yaleksandrova.tm.repository;

import ru.yaleksandrova.tm.api.ICommandRepository;
import ru.yaleksandrova.tm.constant.ApplicationConst;
import ru.yaleksandrova.tm.constant.ArgumentConst;
import ru.yaleksandrova.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    public static final Command ABOUT = new Command(
            ApplicationConst.ABOUT, ArgumentConst.ABOUT, "Display developer info..."
    );

    public static final Command HELP = new Command(
            ApplicationConst.HELP, ArgumentConst.HELP, "Display list of commands..."
    );

    public static final Command VERSION = new Command(
            ApplicationConst.VERSION, ArgumentConst.VERSION, "Display program version..."
    );

    public static final Command INFO = new Command(
            ApplicationConst.INFO, ArgumentConst.INFO, "Display system information..."
    );

    public static final Command EXIT = new Command(
            ApplicationConst.EXIT, null, "Close application..."
    );

    public static final Command ARGUMENTS = new Command(
            ApplicationConst.ARGUMENTS, ArgumentConst.ARGUMENTS, "Display list arguments..."
    );

    public static final Command COMMANDS = new Command(
            ApplicationConst.COMMANDS, ArgumentConst.COMMANDS, "Display list commands..."
    );

    public static final Command[] APPLICATION_COMMANDS = new Command[] {
            ABOUT, HELP, VERSION, INFO, ARGUMENTS, COMMANDS, EXIT
    };

    public Command[] getCommands() {
        return APPLICATION_COMMANDS;
    }
}
