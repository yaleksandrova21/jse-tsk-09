package ru.yaleksandrova.tm.service;

import ru.yaleksandrova.tm.api.ICommandRepository;
import ru.yaleksandrova.tm.api.ICommandService;
import ru.yaleksandrova.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getCommands() {
        return commandRepository.getCommands();
    }

}
